cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.cordova.plugins.cookiemaster/www/cookieMaster.js",
        "id": "com.cordova.plugins.cookiemaster.cookieMaster",
        "pluginId": "com.cordova.plugins.cookiemaster",
        "clobbers": [
            "cookieMaster"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.0",
    "com.cordova.plugins.cookiemaster": "1.0.0"
}
// BOTTOM OF METADATA
});