/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        /*
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        var informacion="Tu SO es: " + device.platform + " Version: " + device.version;

        alert(informacion);

        console.log('Received Event: ' + id);*/

        //la variable elemento guarda una referencia a un elemento

//div donde se muestran las propiedades obtenidas con PhoneGap

//var elemento = document.getElementById('propiedades');

//la variable informacion guarda todas las propiedades obtenidas con PhoneGap

//var informacion='Nombre del dispositivo ' + device.name + '<br />' +  'Version de Cordova: ' + device.cordova + '<br />' + 'Sistema operativo: ' + device.platform + '<br />' + 'Version: ' + device.version + '<br />'+ 'Identificador unico universal: ' + device.uuid + '<br />';

//elemento.innerHTML = informacion;

    }
};

app.initialize();

/**
Funciones para llenar datos... a darle.
*/

var storage = window.localStorage;
if(storage.getItem('token') === null)
    window.location.replace('login.html');

$(document).ready(function() {
    $("#menu_logout").on('click', function(){
        show_loading();
        storage.removeItem('token');
        window.location.replace('login.html');
    });
});

function show_loading(){
    $('.preloader-outer').css('display', 'block');
}

function hide_loading(){
    $('.preloader-outer').css('display', 'none');
}

function showBlogs( data ){
    $('.content-page').html('');
    var data = [
                {
                    'title':'Lorem Ipsum', 
                    'author':'Ala Este-Admin', 
                    'date':' 26/sep/2016 01:45 PM', 
                    'blog_name':' Noticias y Avisos', 
                    'sumary':'Morbi ornare, elit id auctor accumsan, magna felis condimentum metus, eu rutrum dolor leo sit amet metus.', 
                    'blog_id':'123', 
                    'section_id':'123', 
                    'comments':'3'},
                    {
                    'title':'Lorem Ipsum', 
                    'author':'Ala Este-Admin', 
                    'date':' 26/sep/2016 01:45 PM', 
                    'blog_name':' Noticias y Avisos', 
                    'sumary':'Morbi ornare, elit id auctor accumsan, magna felis condimentum metus, eu rutrum dolor leo sit amet metus.', 
                    'blog_id':'123', 
                    'section_id':'123', 
                    'comments':'3'},
                    {
                    'title':'Lorem Ipsum', 
                    'author':'Ala Este-Admin', 
                    'date':' 26/sep/2016 01:45 PM', 
                    'blog_name':' Noticias y Avisos', 
                    'sumary':'Morbi ornare, elit id auctor accumsan, magna felis condimentum metus, eu rutrum dolor leo sit amet metus.', 
                    'blog_id':'123', 
                    'section_id':'123', 
                    'comments':'3'},
                    {
                    'title':'Lorem Ipsum', 
                    'author':'Ala Este-Admin', 
                    'date':' 26/sep/2016 01:45 PM', 
                    'blog_name':' Noticias y Avisos', 
                    'sumary':'Morbi ornare, elit id auctor accumsan, magna felis condimentum metus, eu rutrum dolor leo sit amet metus.', 
                    'blog_id':'123', 
                    'section_id':'123', 
                    'comments':'3'},
            ];

    $.each(data, function(i, v){

        paragraph = '<p> Por: [' + v.author + '] - ' + v.date + ' | Blog: <a class="blog_section" data-sectid="' + v.section_id + '"> ' + v.blog_name + '</a></p>';
        div_clone = $('#div_base').clone();
        div_clone.find('h2 .blog_title').data('blog', v.blog_id);
        div_clone.find('h2 .blog_title').html(v.blog_name);
        div_clone.find('.authoring').html(paragraph);
        div_clone.find('.post-content').html('<p>' + v.sumary + '</p>');
        div_clone.find('.labels').find('.comments-count').html('<a class="link_view"- data-blog_id="' + v.blog_id + '" ><span class="badge"><span class="glyphicon glyphicon-eye-open"></span> Ver</span></a>');
        div_clone.find('.more-links').find('.add_comment').html('<a class="link_comment" data-blog_id="' + v.blog_id + '" ><span class="badge"><span class="glyphicon glyphicon-comment"></span>     ' + v.comments + '</span></a>');
        div_clone.removeAttr('style');
        div_clone.removeAttr("id");
        div_clone.appendTo("#content-page");        
    });
}

function get_blog_single( blog_id, comments ){
    
          $("#this_show_blog").trigger('click'); 

          if(comments == 1)
            $("#go_to_comments").trigger('click');
}

function hide_this(element) {
    if ( element.css('right') == '0px')
        element.stop().animate({'right':'-50px'},1000);
    else
        element.stop().animate({'right':'0px'},1000);
}


function get_edo_cnt(){
    
    $(".content-page").html('');
    var data = {'saldoinicial':'$ 100.00',
                'signosaldoinicial':'0',
                'saldofinal':'$ 10,000.00',
                'signosaldofinal':'+',
                'data':[
                        {
                            'tipo':'adeudo',
                            'concepto':'Suscripcion Anual', 
                            'fecha':'20-01-2015', 
                            'importe':"$ 1,653.00", 
                            'recargo':'$ 0.00', 
                            'trecargo':'$ 1,653.00',
                            'descuento':'$ 0.00', 
                            'ingreso':'$ 1,653.00', 
                            'saldo':'$ 0.00', 
                            'clasificacion':'SUSCRIPCIONES ANUALES DEPOSITO MXM ',
                            'fondo':'DEPOSITOS PESOS MEXICANOS',
                            'aplicado':'',
                            'aplicaciones':[]
                        },
                        {
                            'tipo':'abono',
                            'depto':'Casa LAMARTINE 138', 
                            'monto':'$1,653.00', 
                            'pago':'TB', 
                            'referencia':'FACTURA TB10234', 
                            'fecha':'01-12-2015', 
                            'cuenta':'Scotia Bank Inverlat - MXM', 
                            'fondo':'', 
                            'comment':'Suscripción Anual MXM',
                            'aplicado':'$1,653.00',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripción Anual MXM',
                                                'abono':'$1,653.00',
                                                'aplicado':'$1,653.00',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'adeudo',
                            'concepto':'Casa/House Apto David por Mantenimiento mensual', 
                            'fecha':'31-10-2016', 
                            'importe':"$ 1'000.00", 
                            'recargo':'$ 0.00', 
                            'trecargo':"$ 1'000.00",
                            'descuento':'$ 0.00', 
                            'ingreso':'$ 0.00', 
                            'saldo':'$ 500.00', 
                            'clasificacion':'a. Cuotas de mantenimiento',
                            'fondo':'Fondo ordiario',
                            'aplicado':'',
                            'aplicaciones':[]
                        },
                        {
                            'tipo':'adeudo',
                            'concepto':'Casa/House Apto David por Mantenimiento mensual', 
                            'fecha':'31-10-2016', 
                            'importe':"$ 1'000.00", 
                            'recargo':'$ 0.00', 
                            'trecargo':"$ 1'000.00",
                            'descuento':'$ 0.00', 
                            'ingreso':'$ 0.00', 
                            'saldo':'$ 500.00', 
                            'clasificacion':'a. Cuotas de mantenimiento',
                            'fondo':'Fondo ordiario',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'abono',
                            'depto':'Casa VICENTE BERISTAIN #166', 
                            'monto':'$ 500.00', 
                            'pago':'TDC', 
                            'referencia':'1RV243249K124280F', 
                            'fecha':'14-01-2016', 
                            'cuenta':'PAYPAL - PESOS MEXICANOS', 
                            'fondo':'Paypal Pesos', 
                            'comment':'Pago con PayPal (Vivook) - VICENTE BERISTAIN #166',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'abono',
                            'depto':'Casa VICENTE BERISTAIN #166', 
                            'monto':'$ 500.00', 
                            'pago':'TDC', 
                            'referencia':'1RV243249K124280F', 
                            'fecha':'14-01-2016', 
                            'cuenta':'PAYPAL - PESOS MEXICANOS', 
                            'fondo':'Paypal Pesos', 
                            'comment':'Pago con PayPal (Vivook) - VICENTE BERISTAIN #166',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'abono',
                            'depto':'Casa VICENTE BERISTAIN #166', 
                            'monto':'$ 500.00', 
                            'pago':'TDC', 
                            'referencia':'1RV243249K124280F', 
                            'fecha':'14-01-2016', 
                            'cuenta':'PAYPAL - PESOS MEXICANOS', 
                            'fondo':'Paypal Pesos', 
                            'comment':'Pago con PayPal (Vivook) - VICENTE BERISTAIN #166',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'adeudo',
                            'concepto':'Casa/House Apto David por Mantenimiento mensual', 
                            'fecha':'31-10-2016', 
                            'importe':"$ 1'000.00", 
                            'recargo':'$ 0.00', 
                            'trecargo':"$ 1'000.00",
                            'descuento':'$ 0.00', 
                            'ingreso':'$ 0.00', 
                            'saldo':'$ 500.00', 
                            'clasificacion':'a. Cuotas de mantenimiento',
                            'fondo':'Fondo ordiario',
                            'aplicado':'',
                            'aplicaciones':[]
                        },
                        {
                            'tipo':'abono',
                            'depto':'Casa VICENTE BERISTAIN #166', 
                            'monto':'$ 500.00', 
                            'pago':'TDC', 
                            'referencia':'1RV243249K124280F', 
                            'fecha':'14-01-2016', 
                            'cuenta':'PAYPAL - PESOS MEXICANOS', 
                            'fondo':'Paypal Pesos', 
                            'comment':'Pago con PayPal (Vivook) - VICENTE BERISTAIN #166',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'adeudo',
                            'concepto':'Casa/House Apto David por Mantenimiento mensual', 
                            'fecha':'31-10-2016', 
                            'importe':"$ 1'000.00", 
                            'recargo':'$ 0.00', 
                            'trecargo':"$ 1'000.00",
                            'descuento':'$ 0.00', 
                            'ingreso':'$ 0.00', 
                            'saldo':'$ 500.00', 
                            'clasificacion':'a. Cuotas de mantenimiento',
                            'fondo':'Fondo ordiario',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                        {
                            'tipo':'abono',
                            'depto':'Casa VICENTE BERISTAIN #166', 
                            'monto':'$ 500.00', 
                            'pago':'TDC', 
                            'referencia':'1RV243249K124280F', 
                            'fecha':'14-01-2016', 
                            'cuenta':'PAYPAL - PESOS MEXICANOS', 
                            'fondo':'Paypal Pesos', 
                            'comment':'Pago con PayPal (Vivook) - VICENTE BERISTAIN #166',
                            'aplicado':'$ 404.55',
                            'aplicaciones':[
                                            {
                                                'concepto':'Suscripcion mes de Septiembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Octubre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            },
                                            {
                                                'concepto':'Suscripcion mes de Noviembre 2016',
                                                'abono':'$ 404.55',
                                                'aplicado':'$ 404.55',
                                                'cargo':'0'
                                            }
                                        ]
                        },
                    ]
                };

    div_header = "<div class='resulset-header col-xs-12 col-lg-12'>";
    div_header += "  <div class='resulset-header-row col-xs-6 col-lg-6'>Concepto</div>";
    div_header += "  <div class='resulset-header-row col-xs-6 col-lg-6'>Monto</div>";
    div_header += "</div>";

    $('.content-page').append(div_header);

    $.each(data.data, function(i, v){
        if( v.tipo == 'abono') 
            $('.content-page').append( process_div_abono(v, i) );

        if( v.tipo == 'adeudo') 
            $('.content-page').append( process_div_adeudo(v, i) );
        
    });

    $('.row-saldo-inicial').html(data.saldoinicial);
    $('.row-saldo-final').html(data.saldofinal);

    if(data.signosaldoinicial == '-' )
        $('<div />', {'class': 'row-saldo-inicial saldo-negativo'}).html(data.saldoinicial).appendTo('.content-page');

    if(data.signosaldoinicial == '+' )
        $('<div />', {'class': 'row-saldo-inicial saldo-favor'}).html(data.saldoinicial).appendTo('.content-page');

    if(data.signosaldoinicial == 0 )
        $('<div />', {'class': 'row-saldo-inicial saldo-cero'}).html(data.saldoinicial).appendTo('.content-page');

     if(data.signosaldofinal == '-')
        $('<div />', {'class': 'row-saldo-final saldo-negativo'}).html(data.saldofinal).appendTo('.content-page');

    if(data.signosaldofinal == '+' )
        $('<div />', {'class': 'row-saldo-final saldo-favor'}).html(data.saldofinal).appendTo('.content-page');

    if(data.signosaldofinal == 0 )
        $('<div />', {'class': 'row-saldo-final saldo-cero'}).html(data.saldofinal).appendTo('.content-page');

    hide_this($(".row-saldo-final"));
    hide_this($(".row-saldo-inicial"));
}   

function process_div_adeudo( data, x ){
    clone_adeudo = $("#edocnt_clone_adeudo").clone();

    clone_adeudo.find('.row-title').html( data.concepto + '<br><span class="row-date">' + data.fecha + '</span>' );
    clone_adeudo.find('.resulset-row-importe').html( data.importe + ' <span class="glyphicon glyphicon-fullscreen" data-toggle="modal" data-target="#myModal' + x + '"></span>');
    clone_adeudo.find('.modal').attr('id', 'myModal' + x );
    clone_adeudo.find('.modal-title').html( data.concepto);
    clone_adeudo.find('.date-small').html('Fecha del adeudo: ' + data.fecha);
    clone_adeudo.find('.table-gral-adeudo>tbody').remove();

    tbody = "<tbody>";
    tbody += "  <tr><td class='tdright'>Importe:</td><td class='tdleft'>" + data.importe + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Recargos:</td><td class='tdleft'>" + data.recargo + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Total con recargos:</td><td class='tdleft'>" + data.trecargo + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Descuento:</td><td class='tdleft'>" + data.descuento + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Total ingresos:</td><td class='tdleft'>" + data.ingreso + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Saldo:</td><td class='tdleft'>" + data.saldo + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Clasificación presupuestal:</td><td class='tdleft'>" + data.clasificacion + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Fondo:</td><td class='tdleft'>" + data.fondo + "</td></tr>";
    tbody += "</tbody>";

    clone_adeudo.find('.table-gral-adeudo').html(tbody)
    
    if(data.aplicaciones && data.aplicado != '') {
        table = "<h4>Aplicación</h4>";
        table += "<table class='table table-striped table_aplicaciones'>";
        table += "<thead>";
        table += "  <tr>";
        table += "      <th>Concepto</th>";
        table += "      <th>Aplicaciones</th>";
        table += "  </tr>";
        table += "</thead>";
        tbody = "<tbody>";
        $.each(data.aplicaciones, function(i, val) {
            
            tbody += "  <tr>";
            tbody += "      <td>" + val.concepto + "</td>";
            tbody += "       <td>";
            tbody += "          <span class='abono'>" + val.abono + " <span class='glyphicon glyphicon-plus-sign more'></span></span><br>"; 
            tbody += "           <span class='aplicado'>" + val.aplicado + " <span class='glyphicon glyphicon-ok-sign'></span></span>";

            if( val.cargo != 0){
            tbody += "           <br><span class='aplicado'>" + val.cargo + " <span class='glyphicon glyphicon-minus-sign'></span></span>";
            }

            tbody += "      </td>";
            tbody += "   </tr>";
        });

        tbody += "</tbody>"; 
        table +=tbody;

        tfoot = "<tfoot>";
        tfoot += "  <tr><td colspan='4'>Total aplicado: " + data.aplicado + "</td></tr>";
        tfoot += "</tfoot>";

        table += tfoot;
        table += '</table>';

        clone_adeudo.find('.modal-body').append(table);
    }

    clone_adeudo.removeAttr('id'); 
    clone_adeudo.removeAttr('style'); 

    return clone_adeudo;
}

function process_div_abono( data, x ){
    clone_abono = $("#edocnt_clone_abono").clone();

    clone_abono.find('.row-title').html(data.depto + '<br> <span class="row-date">' + data.fecha + '</span>');
    clone_abono.find('.resulset-row-importe').html(data.monto + ' <span class="glyphicon glyphicon-fullscreen" data-toggle="modal" data-target="#myModal' + x + '"></span>');
    clone_abono.find('.modal').attr('id', 'myModal' + x );
    clone_abono.find('.modal-title').html(data.depto);

    tbody = "<tbody>";
    tbody += "  <tr><td class='tdright'>Monto:</td> <td class='tdleft'>" + data.monto + "</td></tr>";
    tbody += "  <tr><td class='tdright'>Forma de pago:</td><td class='tdleft'>" + data.pago + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Referencia:</td><td class='tdleft'>" + data.referencia + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Fecha movimiento:</td><td class='tdleft'>" + data.fecha + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Cuenta bancaria:</td><td class='tdleft'>" + data.cuenta + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Fondo:</td><td class='tdleft'>" + data.fondo + "</td></tr>";
    tbody += "   <tr><td class='tdright'>Comentarios:</td><td class='tdleft'>" + data.comment + "</td></tr>";
    tbody += "</tbody>";

    clone_abono.find('.table-gral-abono>tbody').remove();
    clone_abono.find('.table-gral-abono').append(tbody);

     if(data.aplicaciones  && data.aplicado != '') {
        table = "<h4>Aplicación</h4>";
        table += "<table class='table table-striped table_aplicaciones'>";
        table += "<thead>";
        table += "  <tr>";
        table += "      <th>Concepto</th>";
        table += "      <th>Aplicaciones</th>";
        table += "  </tr>";
        table += "</thead>";
        tbody = "<tbody>";

        $.each(data.aplicaciones, function(i, val) {
            
            tbody += "  <tr>";
            tbody += "      <td>" + val.concepto + "</td>";
            tbody += "       <td>";
            tbody += "          <span class='abono'>" + val.abono + " <span class='glyphicon glyphicon-plus-sign more'></span></span><br>"; 
            tbody += "           <span class='aplicado'>" + val.aplicado + " <span class='glyphicon glyphicon-ok-sign'></span></span>";

            if( val.cargo != 0){
            tbody += "           <br><span class='aplicado'>" + val.cargo + " <span class='glyphicon glyphicon-minus-sign'></span></span>";
            }

            tbody += "      </td>";
            tbody += "   </tr>";
        });
        
        tbody += "</tbody>"; 
        table +=tbody;

        tfoot = "<tfoot>";
        tfoot += "  <tr><td colspan='4'>Total aplicado: " + data.aplicado + "</td></tr>";
        tfoot += "</tfoot>";

        table += tfoot;
        table += '</table>';

        clone_abono.find('.modal-body').append(table);
    }

    clone_abono.removeAttr('id');
    clone_abono.removeAttr('style');
    return clone_abono;
}
